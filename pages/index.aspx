﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="pages_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MOTEL DA PAIXÃO</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />


    <script src="../bower_components/js/jquery-1.12.0.min.js"></script>
    <script src="../bower_components/js/bootstrap.min.js"></script>

    <!-- Google Font -->
    <link href="../bower_components/font-awesome/fonte.css" rel="stylesheet" />

</head>


<body class="hold-transition skin-blue layout-top-nav">


    <style type="text/css">
        .radioButtonList {
            list-style: none;
            margin: 0;
            padding: 0;
        }

            .radioButtonList.horizontal li {
                display: inline;
            }

            .radioButtonList label {
                display: inline;
            }
    </style>


    <form id="form1" runat="server">
        <div>
            <div class="wrapper">

                <header class="main-header">
                    <nav class="navbar navbar-static-top">
                        <div class="container">
                            <div class="navbar-header">
                                <a href="index.aspx" class="navbar-brand"><b>MOTEL DA PAIXÃO</b></a>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                                <ul class="nav navbar-nav pull-right">
                                    <%--<li><a href="#">Link</a></li>--%>
                                </ul>

                            </div>


                            <div class="navbar-custom-menu">
                                <ul class="nav navbar-nav">


                                    <!-- Tasks Menu -->
                                    <li class="dropdown tasks-menu">
                                        <!-- Menu Toggle Button -->
                                        <a href="/pages/login.aspx">
                                            <i class="fa fa-user-circle"></i>
                                            <span class="label label-danger"></span>
                                        </a>
                                    </li>

                                </ul>
                            </div>


                            <!-- /.navbar-collapse -->
                            <!-- Navbar Right Menu -->

                            <!-- /.navbar-custom-menu -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </header>

                <!-- Full Width Column -->
                <div class="content-wrapper">
                    <div class="container">


                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <h1>Reserva
                            <small></small>
                            </h1>
                            <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                        </section>
                        <!-- Main content -->

                        <section class="content col-lg-12">
                            <div class="col-lg-6">
                                <div class="register-box">
                                    <div class="form-group has-feedback">
                                        <label>Data de Entrada / Horario de entrada</label>
                                        <asp:TextBox ID="txtEntrada" runat="server" CssClass="form-control col-lg-3" type="datetime-local" required=""></asp:TextBox>

                                    </div>
                                    &nbsp;
                                <div class="form-group has-feedback">
                                    <label>Data de Saida  / Horario de entrada</label>
                                    <asp:TextBox ID="txtSaida" runat="server" CssClass="form-control col-lg-3" type="datetime-local" required=""></asp:TextBox>
                                </div>

                                    &nbsp;
                                <div class="form-group has-feedback">
                                    <label>Quantidade de Pessoas</label>
                                    <asp:TextBox ID="txtQtdPessoas" runat="server" CssClass="form-control col-lg-3" type="number" required=""></asp:TextBox>
                                </div>
                                    &nbsp;
                                        <div class="form-group has-feedback">
                                            <label>Pagamento</label>
                                            <asp:RadioButtonList ID="rblPagamento" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" required="">
                                                <asp:ListItem Value="c" Selected="True">Cartão</asp:ListItem>
                                                <asp:ListItem Value="b">Boleto</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>


                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div class="register-box">
                                    <div class="form-group has-feedback">
                                        <label>Escolha o quarto</label>
                                        <asp:DropDownList ID="dplQuartos" runat="server" CssClass="form-control dropdown" OnSelectedIndexChanged="dplQuartos_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="0">Escolha um quarto</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    
                                    
                                    

                                    <div class="row">
                                        <div class="form-group col-lg-5 pull-right">
                                            <asp:Button ID="btnProximo" runat="server" Text="Reservar" CssClass="btn btn-primary btn-block btn-flat" OnClick="btnProximo_Click" />
                                        </div>
                                    </div>


                                </div>

                                <asp:GridView ID="gridQuarto" runat="server" CssClass="table " AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            
                                            
                                            <asp:BoundField DataField="qua_descricao" HeaderText="Descricao" />
                                            <asp:BoundField DataField="qua_num_usu_suportados" HeaderText="Pessoas" />
                                            <asp:BoundField DataField="qua_banheiro" HeaderText="Banheiros" />
                                            <asp:BoundField DataField="qua_hidro" HeaderText="Hidro" />
                                            <asp:BoundField DataField="qua_qt_cama" HeaderText="Camas" />
                                            <asp:BoundField DataField="qua_valor" HeaderText="Valor R$" />


                                          
                                        </Columns>
                                        <EditRowStyle BackColor="#2461BF" />
                                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#2461BF" />
                                        <RowStyle BackColor="#EFF3FB" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    </asp:GridView>

                            </div>
                        </section>


                        <!-- /.form-box -->
                    </div>

                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="container">
                    <div class="pull-right hidden-xs">
                        <b>FATEC</b> Guaratinguetá
                    </div>
                    <strong>Copyright &copy; 2017 <a href="#">MOTEL DA PAIXÃO</a>.</strong> All rights
      reserved.
                </div>
                <!-- /.container -->
            </footer>
        </div>
        <!-- ./wrapper -->

        <script src="../../dist/js/adminlte.min.js"></script>
    </form>

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 4;
            setTimeout(function () {
                document.getElementById("<%=lblMsg.ClientID%>").style.display = "none";
        }, seconds * 1000);
        };
    </script>

</body>
</html>
