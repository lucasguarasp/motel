﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="pages_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login</title>


    <!-- Bootstrap 3.3.7 -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/square/blue.css" />

    <link href="../bower_components/font-awesome/fonte.css" rel="stylesheet" />

</head>


<body class="hold-transition login-page">

    <form id="form1" runat="server">

        <div class="login-box">
            <div class="login-logo">
                <a href="index.aspx"><b>Login</b> MP</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Entre com seus dados</p>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtLogin" runat="server" type="text" CssClass="form-control" placeholder="Login" required=""></asp:TextBox>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtSenha" runat="server" type="password" CssClass="form-control" placeholder="Senha" required=""></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" />
                                Lembrar
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <asp:Button ID="btnEntrar" runat="server" Text="Entrar" type="submit" CssClass="btn btn-primary btn-block btn-flat" OnClick="btnEntrar_Click"/>
                    </div>
                    <!-- /.col -->
                    <div class="col-lg-12">
                        <asp:TextBox ID="txtId" runat="server" Visible="false"></asp:TextBox>
                        <asp:Label ID="lblMensagem" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>


                <%--                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i>Sign in using
        Facebook</a>
                    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i>Sign in using
        Google+</a>
                </div>
                <!-- /.social-auth-links -->

                <a href="#">I forgot my password</a><br>
                <a href="register.html" class="text-center">Register a new membership</a>--%>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="../plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>

         <script type="text/javascript">
             function HideLabel() {
                 var seconds = 5;
                 setTimeout(function () {
                     document.getElementById("<%=lblMensagem.ClientID %>").style.display = "none";
                 }, seconds * 1000);
             };
    </script>

    </form>
</body>
</html>
