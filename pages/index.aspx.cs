﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            CarregarDDL();
            CarregaGrid(0);
        }
        lblMsg.Visible = false;
    }

    protected void btnProximo_Click(object sender, EventArgs e)
    {
      

        DateTime date = new DateTime();

        Reserva reserva = new Reserva();
        reserva.Res_horario_entrada = Convert.ToDateTime(txtEntrada.Text);
        reserva.Res_horario_saida = Convert.ToDateTime(txtSaida.Text);
        reserva.Res_dt_cadastro_reserva = date;
        reserva.Res_numero_pessoas = Convert.ToInt32(txtQtdPessoas.Text);
        reserva.Res_tipo_pagamento = rblPagamento.SelectedValue;

        Quarto quarto = new Quarto();
        quarto.Qua_id = Convert.ToInt32(dplQuartos.SelectedValue);
        reserva.Qua_quarto = quarto;
        


        switch (ReservaDB.Insert(reserva))
        {
            case 0:
                lblMsg.Visible = true;
                lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Cadastrado com sucesso</div>";
                ReservaDB update = new ReservaDB();
                update.UpdateStatusQuarto0(reserva.Qua_quarto);
               
                break;
            case -2:

                lblMsg.Visible = true;
                lblMsg.Text = "</br></br> <div class='alert alert-danger'>Cadastro não pôde ser concluído</div>";
                
                break;
        }

        Response.Redirect("registro.aspx");

    }



    public void CarregarDDL()
    {

        //Carregar um DropdownList com o banco de dados
        DataSet ds = ReservaDB.SelectAll();
        dplQuartos.DataSource = ds;
        //nome da coluna do banco de dados;
        dplQuartos.DataTextField = "qua_tipo";
        //ID da coluna do Banco
        dplQuartos.DataValueField = "qua_id";
        dplQuartos.DataBind();
        dplQuartos.Items.Insert(0, "Selecione");


    }

    private void CarregaGrid(int quarto)
    {
        ReservaDB bd = new ReservaDB();
        DataSet ds = bd.Search(quarto);
        gridQuarto.DataSource = ds.Tables[0].DefaultView;
        gridQuarto.DataBind();
        int registros = ds.Tables[0].Rows.Count;
        //if (registros == 0)
        //    lblResultado.Text = "Nenhum dado nesse local";
        //else
        //    lblResultado.Text = "Dados desse local";
    }


    protected void dplQuartos_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dplQuartos.SelectedItem.Text != "Selecione")
        {
            int quarto = Convert.ToInt32(dplQuartos.SelectedItem.Value);
            CarregaGrid(quarto);
        }
        else
        {
            CarregaGrid(0);
        }
    }
}