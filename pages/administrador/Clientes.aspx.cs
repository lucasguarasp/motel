﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_Clientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }

    protected void gridClientes_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    private void CarregarGrid()
    {

        DataSet ds = UsuarioDB.SelectClientes2();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridClientes.DataSource = ds.Tables[0].DefaultView;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            //lbl.Text = "Foram encontrados " + qtd + " de registros";
        }
        else
        {
            gridClientes.Visible = false;
            //lbl.Text = "Não foram encontrado registros...";
        }

    }
}