﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="cadastrarQuartos.aspx.cs" Inherits="pages_administrador_cadastrarQuartos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastro de quartos</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">            
                    
            <div class="row col-lg-12">
                <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                <div class="form-group has-feedback">
                    <label>Tipo</label>
                    <asp:TextBox ID="txtTipo" runat="server" CssClass="form-control col-lg-3" type="text"></asp:TextBox>

                </div>
                &nbsp;
                <div class="form-group has-feedback">
                    <label>Descrição</label>
                    <asp:TextBox ID="txtDescricao" runat="server" Columns="170" Rows="5" CssClass="form-control"></asp:TextBox>

                </div>

                <div class="row">
                    <div class="form-group col-lg-4">
                        <label>Nº de usuarios suportardo</label>
                        <asp:TextBox ID="txtQtdPessoas" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Qtd de banheiro</label>
                        <asp:TextBox ID="txtQtdBanheiro" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Qtd de chuveiro</label>
                        <asp:TextBox ID="txtQtdChuveiro" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Qtd de hidromassagem</label>
                        <asp:TextBox ID="txtHidro" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Qtd de brinquedo</label>
                        <asp:TextBox ID="txtQtdBrinquedo" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Qtd de cama</label>
                        <asp:TextBox ID="txtQtdCama" runat="server" CssClass="form-control" type="number"></asp:TextBox>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group col-lg-10 pull-left">
                        <label>Disponivel?</label>
                        <asp:RadioButtonList ID="rblDisponivel" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">sim</asp:ListItem>
                            <asp:ListItem Value="0">Não</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                    <div class="form-group col-lg-2 pull-right">
                        <label>Valor</label>
                        <asp:TextBox ID="txtValor" runat="server" CssClass="form-control" type="text"></asp:TextBox>
                    </div>


                </div>
                <div class="col-lg-12">
                    <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" CssClass="btn btn-success btn-flat pull-right" />
                </div>



            </div>
            <!-- /.row -->
        </div>
    </div>

    <script type="text/javascript">
    function HideLabel() {
        var seconds = 4;
        setTimeout(function () {
            document.getElementById("<%=lblMsg.ClientID%>").style.display = "none";
        }, seconds * 1000);
    };
        </script>

</asp:Content>

