﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="Clientes.aspx.cs" Inherits="pages_administrador_Clientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Clientes</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row col-lg-12">

                <div class="box-body">
                    <asp:GridView ID="gridClientes" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gridClientes_RowCommand" DataKeyNames="cli_id">

                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                        <Columns>
                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/bower_components/Ionicons/png/512/search.png" ShowSelectButton="True">
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:CommandField>
                            <asp:BoundField DataField="cli_id" HeaderText="ID" />
                            <asp:BoundField DataField="cli_nome" HeaderText="Nome" />
                            <asp:BoundField DataField="cli_dt_cadstro" HeaderText="Data de cadastro" />
                            <asp:BoundField DataField="cli_sexo" HeaderText="Sexo" />
                            <asp:BoundField DataField="cli_email" HeaderText="Email" />
                            <asp:BoundField DataField="cli_telefone" HeaderText="Telefone" />

                            <asp:ButtonField HeaderText="" CommandName="alterar" ButtonType="Button" Text="Alterar" ControlStyle-CssClass="btn btn-warning">
                                <ControlStyle CssClass="btn btn-warning"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField HeaderText="" CommandName="deletar" ButtonType="Button" Text="Apagar" ControlStyle-CssClass="btn btn-danger">

                                <ControlStyle CssClass="btn btn-danger"></ControlStyle>
                            </asp:ButtonField>

                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </div>
                <!-- /.FIM GRID -->

            </div>
            <!-- /.row -->
        </div>
    </div>

</asp:Content>

