﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_cadastrarQuartos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Visible = false;

    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {
        lblMsg.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

        Quarto quarto = new Quarto();
        quarto.Qua_tipo = txtTipo.Text;
        quarto.Qua_descricao = txtDescricao.Text;
        quarto.Qua_num_usuportados = Convert.ToInt32(txtQtdPessoas.Text);
        quarto.Qua_banheiro = Convert.ToInt32(txtQtdBanheiro.Text);
        quarto.Qua_chuveiro = Convert.ToInt32(txtQtdChuveiro.Text);
        quarto.Qua_hidro = Convert.ToInt32(txtHidro.Text);
        quarto.Qua_brinquedos = Convert.ToInt32(txtQtdBrinquedo.Text);
        quarto.Qua_qt_cama = Convert.ToInt32(txtQtdCama.Text);
        quarto.Qua_status = Convert.ToInt32(rblDisponivel.SelectedValue);
        quarto.Qua_valor = Convert.ToDouble(txtValor.Text);
        

        switch (ReservaDB.Insert(quarto))
        {
            case 0:
                lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Cadastrado com sucesso</div>";                

                txtTipo.Text = "";
                txtDescricao.Text = "";
                txtQtdPessoas.Text = "";
                txtQtdBanheiro.Text = "";
                txtQtdChuveiro.Text = "";
                txtHidro.Text = "";
                txtQtdBrinquedo.Text = "";
                txtQtdCama.Text = "";
                txtValor.Text = "";
                break;
            case -2:
                lblMsg.Text = "</br></br> <div class='alert alert-danger'>Cadastro não pôde ser concluído</div>";

                txtTipo.Text = "";
                txtDescricao.Text = "";
                txtQtdPessoas.Text = "";
                txtQtdBanheiro.Text = "";
                txtQtdChuveiro.Text = "";
                txtHidro.Text = "";
                txtQtdBrinquedo.Text = "";
                txtQtdCama.Text = "";
                txtValor.Text = "";
                break;
        }

    }


}