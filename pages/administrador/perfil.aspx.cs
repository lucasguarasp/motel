﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_perfil : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtNome.Text = Session["nome"].ToString();
            txtLogin.Text = Session["login"].ToString();
            txtSenha.Text = Session["senha"].ToString();
        }
        
    }

    protected void btnEditarPerfil_Click(object sender, EventArgs e)
    {
        txtNome.Enabled = true;
        txtLogin.Enabled = true;
        txtSenha.Enabled = true;
        ddlPerfil.Enabled = true;

    }

    protected void btnSalvarPerfil_Click(object sender, EventArgs e)
    {

        lblMsg.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);


        Usuario usuario = new Usuario();
        usuario.Usu_nome = txtNome.Text;
        usuario.Usu_login = txtLogin.Text;
        usuario.Usu_id = Convert.ToInt32(Session["id"]);  //pega a id salva na session


        TipoUsuario tip = new TipoUsuario();
        tip.Tip_tipo_id = Convert.ToInt32(ddlPerfil.SelectedValue);
        usuario.Tip_tipo_usuario_tip_tipo_id = tip;

        switch (UsuarioDB.UpdatePerfil(usuario))
        {
            case 0:
                Session["nome"] = txtNome.Text;
                Label lbl = (Label)Master.FindControl("lblNome");


                if (lbl != null)
                {
                    lbl.Text = txtNome.Text;
                }
                

                //lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Atualizado com sucesso</div>";
                lblMsg.Text = "<div class='alert span4 alert-success'>Atualizado com sucesso</div>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>$('#myModal').modal('show');</script>", false);

                break;
            case -2:
                //lblMsg.Text = "</br></br> <div class='alert alert-danger'>Não pôde ser atualizado</div>";
                lblMsg.Text = "<div class='callout callout-danger'>Não pôde ser atualizado</div>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>$('#myModal').modal('show');</script>", false);
                break;
        }

        txtNome.Enabled = false;
        txtLogin.Enabled = false;
        ddlPerfil.Enabled = false;
    }
}