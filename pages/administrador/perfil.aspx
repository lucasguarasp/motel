﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="perfil.aspx.cs" Inherits="pages_administrador_perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .espaco {
            margin-top: 10px;
        }
    </style>


    <div class="box box-info" />
        <div class="box-header with-border">
            <h3 class="box-title">Perfil</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNome" runat="server" placeholder="Nome" Enabled="false" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label espaco">Login</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtLogin" runat="server" Enabled="false" CssClass="form-control" placeholder="Login"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label espaco">Senha</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtSenha" runat="server" Enabled="false" CssClass="form-control" placeholder="Senha"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perfil</label>
                        <div class="col-sm-10">
                            <asp:DropDownList ID="ddlPerfil" runat="server" Enabled="false" CssClass="form-control">
                                <asp:ListItem Value="1">Administrador</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer espaco">
                <asp:Button ID="btnEditarPerfil" runat="server" Text="Editar" class="btn btn-default" OnClick="btnEditarPerfil_Click" />
                <asp:Button ID="btnSalvarPerfil" runat="server" Text="Salvar" class="btn btn-info pull-right" OnClick="btnSalvarPerfil_Click"/>

            </div>
            <div class="col-lg-12">
                <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
            </div>            


        </div>

         <script type="text/javascript">
             function HideLabel() {
                 var seconds = 5;
                 setTimeout(function () {
                     document.getElementById("<%=lblMsg.ClientID %>").style.display = "none";
             }, seconds * 1000);
             };
    </script>

</asp:Content>

