﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="VisualizarQuarto.aspx.cs" Inherits="pages_administrador_VisualizarQuarto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Visualizar Quartos</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row col-lg-12">


                <asp:GridView ID="gridQuartos" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gridQuartos_RowCommand" DataKeyNames="qua_id">

                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                    <Columns>
                        <asp:BoundField DataField="qua_tipo" HeaderText="Nome" />
                        <asp:BoundField DataField="qua_status" HeaderText="Disponível" />
                        <asp:BoundField DataField="qua_descricao" HeaderText="Descrição" />
                        <asp:BoundField DataField="qua_num_usu_suportados" HeaderText="Pessoas" />
                        <asp:BoundField DataField="qua_banheiro" HeaderText="Banheiro" />
                        <asp:BoundField DataField="qua_chuveiro" HeaderText="Chuveiro" />
                        <asp:BoundField DataField="qua_hidro" HeaderText="Hidro" />
                        <asp:BoundField DataField="qua_brinquedos" HeaderText="Brinquedps" />
                        <asp:BoundField DataField="qua_qt_cama" HeaderText="Cama" />
                        <asp:BoundField DataField="qua_valor" HeaderText="Valor" />

                      
                        <asp:ButtonField HeaderText="" CommandName="sim" ButtonType="Button" Text="Sim" ControlStyle-CssClass="btn btn-warning">
                            <ControlStyle CssClass="btn btn-success"></ControlStyle>
                        </asp:ButtonField>
                         <asp:ButtonField HeaderText="" CommandName="nao" ButtonType="Button" Text="Não" ControlStyle-CssClass="btn btn-warning">
                            <ControlStyle CssClass="btn btn-warning"></ControlStyle>
                        </asp:ButtonField>



                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>

                <asp:Label ID="lbl" runat="server"></asp:Label>



            </div>
            <!-- /.row -->
        </div>
    </div>


</asp:Content>

