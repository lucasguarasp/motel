﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="administradores.aspx.cs" Inherits="pages_administrador_administradores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Administradores</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-lg-12">
                    <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar Administrador" CssClass="btn btn-facebook pull-right" OnClick="btnCadastrar_Click" />
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                </div>

                <!-- Início - Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel">Cadastro de administrador</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <asp:TextBox ID="txtNome" runat="server" class="form-control" placeholder="Nome completo"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Login</label>
                                        <asp:TextBox ID="txtLogin" runat="server" class="form-control" placeholder="Login"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Senha</label>
                                        <asp:TextBox ID="txtSenha" runat="server" class="form-control" type="password" placeholder="Senha"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Repita senha</label>
                                        <asp:TextBox ID="txtRepitaSenha" runat="server" class="form-control" type="password" placeholder="Repita senha"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <%--<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>--%>
                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="btn btn-success" OnClick="btnSalvar_Click" defaultbutton="btnSalvar" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim - Modal -->


                <!-- Início - Modal -->
                <div class="modal fade" id="modalAlterar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel1">Alterar usuario</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <asp:TextBox ID="txtNomeNew" runat="server" class="form-control" placeholder="Nome completo"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Login</label>
                                        <asp:TextBox ID="txtLoginNew" runat="server" class="form-control" placeholder="Login"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Senha</label>
                                        <asp:TextBox ID="txtSenhaNew" runat="server" class="form-control" type="password" placeholder="Senha"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <%--<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>--%>
                                <asp:Button ID="btnAlterar" runat="server" Text="Salvar" CssClass="btn btn-success" OnClick="btnAlterar_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim - Modal -->



            </div>
            <!-- /.row -->
        </div>


        <!-- /.GRID -->
        <div class="box-body">
            <asp:GridView ID="gridAdmistradores" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gridAdmistradores_RowCommand" DataKeyNames="usu_id">

                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/bower_components/Ionicons/png/512/search.png" ShowSelectButton="True">
                        <ControlStyle Height="16px" Width="16px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="usu_id" HeaderText="ID" />
                    <asp:BoundField DataField="usu_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="usu_login" HeaderText="Login" />
                    <asp:BoundField DataField="Date" HeaderText="Data de cadastro" />

                    <asp:ButtonField HeaderText="" CommandName="alterar" ButtonType="Button" Text="Alterar" ControlStyle-CssClass="btn btn-warning">
                        <ControlStyle CssClass="btn btn-warning"></ControlStyle>
                    </asp:ButtonField>
                    <asp:ButtonField HeaderText="" CommandName="deletar" ButtonType="Button" Text="Apagar" ControlStyle-CssClass="btn btn-danger">
                        <ControlStyle CssClass="btn btn-danger"></ControlStyle>
                    </asp:ButtonField>

                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
        <!-- /.FIM GRID -->

    </div>

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 4;
            setTimeout(function () {
                document.getElementById("<%=lblMsg.ClientID%>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>

