﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_MasterPageAdm : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Contador();
        }



        if (Session["nome"] == null || Session["perfil"] == null)
        {
            Response.Redirect("../login.aspx");
        }
        else
        {
            int perfil = Convert.ToInt32(Session["perfil"]);

            if (perfil == 1)
            {

                lblNome.Text = Session["nome"].ToString();
                lblData.Text = Session["data"].ToString();
            }
            else
            {
                Response.Redirect("../login.aspx");
            }
        }

    }
    
    protected void btnSair_Click(object sender, EventArgs e)
    {
        Session.Remove("nome");
        Session.Remove("perfil");
        Response.Redirect("../login.aspx");
    }


    private void Contador()
    {

        DataSet dsQuartos = ReservaDB.SelectQuartos();
        DataSet dsClientes = UsuarioDB.SelectClientes();
        DataSet dsQDisponivel = Relatorios.SelectQuartosDisponiveis();
        DataSet dsQOcupados = Relatorios.SelectQuartosOcupados();

        int qtd = dsQuartos.Tables[0].Rows.Count;
        int qtd2 = dsQDisponivel.Tables[0].Rows.Count;
        int qtd3 = dsQOcupados.Tables[0].Rows.Count;
        int qtd4 = dsClientes.Tables[0].Rows.Count;


        if (qtd > 0 || qtd2 > 0 || qtd3 > 0 || qtd4 > 0)
        {
            lblQuartos.Text = "" + qtd;
            lblQDisponiveis.Text = "" + qtd2;
            lblQOcupados.Text = "" + qtd3;
            lblClientes.Text = "" + qtd4;
        }
        else
        {
            lblQuartos.Text = "0";
            lblQDisponiveis.Text = "0";
            lblQOcupados.Text = "0";
            lblClientes.Text = "0";
        }

    }


}
