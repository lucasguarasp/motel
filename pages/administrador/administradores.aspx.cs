﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_administradores : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
        lblMsg.Visible = false;
        if (!IsPostBack)
        {
            CarregarGrid();
        }

    }

    private void CarregarGrid()
    {

        DataSet ds = UsuarioDB.SelectUsuarios();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridAdmistradores.DataSource = ds.Tables[0].DefaultView;
            gridAdmistradores.DataBind();
            gridAdmistradores.Visible = true;
            //lbl.Text = "Foram encontrados " + qtd + " de registros";
        }
        else
        {
            gridAdmistradores.Visible = false;
            //lbl.Text = "Não foram encontrado registros...";
        }

    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {

        lblMsg.Visible = false;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>$('#myModal').modal('show');</script>", false);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        lblMsg.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

        if (txtNome.Text != "" && txtLogin.Text != "" && txtSenha.Text != "" && txtSenha.Text == txtRepitaSenha.Text)
        {

            Usuario usuario = new Usuario();
            usuario.Usu_nome = txtNome.Text;
            usuario.Usu_login = txtLogin.Text;
            usuario.Usu_senha = txtSenha.Text;

            TipoUsuario tip = new TipoUsuario();
            tip.Tip_tipo_id = Convert.ToInt32(1);
            usuario.Tip_tipo_usuario_tip_tipo_id = tip;


            switch (UsuarioDB.Insert(usuario))
            {
                case 0:
                    lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Cadastrado com sucesso</div>";
                    CarregarGrid();

                    txtNome.Text = "";
                    txtLogin.Text = "";
                    txtSenha.Text = "";
                    txtRepitaSenha.Text = "";
                    break;
                case -2:
                    lblMsg.Text = "</br></br> <div class='alert alert-danger'>Cadastro não pôde ser concluído</div>";

                    txtNome.Text = "";
                    txtLogin.Text = "";
                    txtSenha.Text = "";
                    txtRepitaSenha.Text = "";
                    break;
            }

        }
        else if (txtSenha.Text != txtRepitaSenha.Text)
        {
            lblMsg.Text = "</br></br> <div class='alert alert-danger'>Senhas diferentes!!!</div>";
        }
        else
        {
            lblMsg.Text = "</br></br> <div class='alert alert-danger'>Preencha os campos</div>";
        }
    }


    protected void gridAdmistradores_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        UpDel.Index = Convert.ToInt32(e.CommandArgument);
        UpDel.Id = Convert.ToInt32(gridAdmistradores.DataKeys[UpDel.Index].Value);

        if (e.CommandName.Equals("alterar"))
        {
            Page.ClientScript.RegisterStartupScript(GetType(), key: "script", script: "<script>$('#modalAlterar').modal('show');</script> ", addScriptTags: false);
        }
        else if (e.CommandName.Equals("deletar"))           
        {
            UsuarioDB db = new UsuarioDB();
            db.Delete(UpDel.Id);
            CarregarGrid();
        }
    }

    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        UsuarioDB db = new UsuarioDB();
        DataSet ds = db.SelectAll();

        Usuario usu = db.Select(UpDel.Id);

        usu.Usu_nome = txtNomeNew.Text;
        usu.Usu_login = txtLoginNew.Text;
        usu.Usu_senha = txtSenhaNew.Text;


        if (db.Update(usu))
        {

            txtNome.Text = "";
            txtLoginNew.Text = "";
            txtSenhaNew.Text = "";
            CarregarGrid();
        }
        else
        {
            lblMsg.Text = "Erro ao atualizar";//Sua label ou div,

        }

    }


}