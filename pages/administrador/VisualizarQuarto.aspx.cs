﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_VisualizarQuarto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }


    private void CarregarGrid()
    {

        DataSet ds = ReservaDB.SelectQuartos();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridQuartos.DataSource = ds.Tables[0].DefaultView;
            gridQuartos.DataBind();
            gridQuartos.Visible = true;
            lbl.Text = "Foram encontrados " + qtd + " de registros";
        }
        else
        {
            gridQuartos.Visible = false;
            lbl.Text = "Não foram encontrado registros...";
        }

    }


    protected void gridQuartos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("sim"))
        {

            //Reserva reserva = new Reserva();
            //UpDel.Index = Convert.ToInt32(e.CommandArgument);
            //UpDel.Id = Convert.ToInt32(gridQuartos.DataKeys[UpDel.Index].Value);

            ////reserva.Qua_quarto = UpDel.Id;

            //ReservaDB update = new ReservaDB();
            //update.UpdateStatusQuarto0(reserva.Qua_quarto);
            //CarregarGrid();

            //UpDel.Index = Convert.ToInt32(e.CommandArgument);
            //UpDel.Id = Convert.ToInt32(gridQuartos.DataKeys[UpDel.Index].Value);
            //ReservaDB reserva = new ReservaDB();
            //reserva.UpdateStatusQuarto1();
            //CarregarGrid();


        }
        else if (e.CommandName.Equals("nao"))
        {
            //Reserva reserva = new Reserva();
            //ReservaDB update = new ReservaDB();
            //update.UpdateStatusQuarto0(reserva.Qua_quarto);
            //CarregarGrid();
        }
    }
}