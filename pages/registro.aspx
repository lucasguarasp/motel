﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="pages_registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MOTEL DA PAIXÃO</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />


    <script src="../bower_components/js/jquery-1.12.0.min.js"></script>
    <script src="../bower_components/js/bootstrap.min.js"></script>

    <!-- Google Font -->
    <link href="../bower_components/font-awesome/fonte.css" rel="stylesheet" />

</head>

<body class="hold-transition skin-blue layout-top-nav">


    <style type="text/css">
        .radioButtonList {
            list-style: none;
            margin: 0;
            padding: 0;
        }

            .radioButtonList.horizontal li {
                display: inline;
            }

            .radioButtonList label {
                display: inline;
            }
    </style>


    <form id="form1" runat="server">
        <div>
            <div class="wrapper">

                <header class="main-header">
                    <nav class="navbar navbar-static-top">
                        <div class="container">
                            <div class="navbar-header">
                                <a href="index.aspx" class="navbar-brand"><b>MOTEL DA PAIXÃO</b></a>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                                <ul class="nav navbar-nav pull-right">
                                    <%--<li><a href="#">Link</a></li>--%>
                                </ul>

                            </div>


                            <div class="navbar-custom-menu">
                                <ul class="nav navbar-nav">


                                    <!-- Tasks Menu -->
                                    <li class="dropdown tasks-menu">
                                        <!-- Menu Toggle Button -->
                                        <a href="/pages/login.aspx">
                                            <i class="fa fa-user-circle"></i>
                                            <span class="label label-danger"></span>
                                        </a>
                                    </li>

                                </ul>
                            </div>


                            <!-- /.navbar-collapse -->
                            <!-- Navbar Right Menu -->

                            <!-- /.navbar-custom-menu -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </header>

                <!-- Full Width Column -->
                <div class="content-wrapper">
                    <div class="container">


                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <h1>Reserva
                            <small></small>
                            </h1>
                        </section>
                        <!-- Main content -->
                        <div class="register-box">
                            <section class="content col-lg-12">

                                <div class="form-group has-feedback">
                                    <label>Nome</label>
                                    <asp:TextBox ID="txtNome" runat="server" CssClass="form-control col-lg-3" type="text" required=""></asp:TextBox>

                                </div>
                                &nbsp;
                                <div class="form-group has-feedback">
                                    <label>Email</label>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control col-lg-3" type="text"></asp:TextBox>
                                </div>

                                &nbsp;
                                <div class="form-group has-feedback">
                                    <label>Telefone</label>
                                    <asp:TextBox ID="txtTelefone" runat="server" CssClass="form-control col-lg-3" type="number"></asp:TextBox>
                                </div>
                                &nbsp;
                                        <div class="form-group has-feedback">
                                            <label>Sexo</label>
                                            <asp:RadioButtonList ID="rblSexo" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList">
                                                <asp:ListItem Value="m">Masculino</asp:ListItem>
                                                <asp:ListItem Value="d">Feminino</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>


                                <div class="row">
                                   
                                        <div class="form-group col-lg-4 pull-left">
                                     <asp:Button ID="btnVoltar" runat="server" Text="Voltar" CssClass="btn btn-default btn-block btn-flat" OnClick="btnVoltar_Click" />
                                        </div>
                                        <div class="form-group col-lg-5 pull-right">
                                    <asp:Button ID="btnReservar" runat="server" Text="Reservar" CssClass="btn btn-primary btn-block btn-flat" OnClick="btnReservar_Click" />
                                        </div>
                                  
                                </div>


                                <div class="col-lg-6">
                                </div>

                            </section>
                        </div>

                        <!-- /.form-box -->
                    </div>

                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="container">
                    <div class="pull-right hidden-xs">
                        <b>FATEC</b> Guaratinguetá
                    </div>
                    <strong>Copyright &copy; 2017 <a href="#">MOTEL DA PAIXÃO</a>.</strong> All rights
      reserved.
                </div>
                <!-- /.container -->
            </footer>
        </div>
        <!-- ./wrapper -->

        <script src="../../dist/js/adminlte.min.js"></script>

    </form>
</body>
</html>
