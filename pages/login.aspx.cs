﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnEntrar_Click(object sender, EventArgs e)
    {
        lblMensagem.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);

        DataSet ds = UsuarioDB.SelectLOGIN(txtLogin.Text, txtSenha.Text);

        if (ds.Tables[0].Rows.Count == 1)
        {
            lblMensagem.Text = "<div class='alert span4 alert-sucess'>OK</div>";
            // Salvando campos em session

            Session["perfil"] = ds.Tables[0].Rows[0]["tip_tipo_usuario_tip_tipo_id"].ToString();
            Session["nome"] = ds.Tables[0].Rows[0]["usu_nome"].ToString();
            Session["data"] = ds.Tables[0].Rows[0]["usu_dt_cadastro"].ToString();
            Session["id"] = ds.Tables[0].Rows[0]["usu_id"].ToString();
            Session["login"] = ds.Tables[0].Rows[0]["usu_login"].ToString();
            Session["senha"] = ds.Tables[0].Rows[0]["usu_senha"].ToString();
            
            int perfil = Convert.ToInt32(Session["perfil"]);
            if (perfil == 1) { Response.Redirect("administrador/Index.aspx"); }
        }
        else
        {
            //Response.Redirect("Erro.aspx");
            lblMensagem.Text = "<div class='alert span4 alert-danger'>Usuário ou Senha inválido</div>";
        }

    }



}