﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de ReservaDB
/// </summary>
public class ReservaDB
{

    public static int Insert(Reserva reserva)
    {
        DateTime data = DateTime.Now;
        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "INSERT INTO res_reserva (res_horario_entrada, res_horario_saida, res_dt_cadastro_reserva, res_numero_pessoas, res_tipo_pagamento, qua_quarto_qua_id) VALUES (?entrada, ?saida, ?dt_cadastro, ?qtd_pessoas, ?tipo_pagamento, ?quarto_id)";
           //             INSERT INTO res_reserva (res_horario_entrada, res_horario_saida, res_dt_cadastro_reserva, res_numero_pessoas, res_tipo_pagamento, qua_quarto_qua_id) VALUES (now(),     now(),  now(),        3,            'c',             1);
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?entrada", reserva.Res_horario_entrada));
            objCommand.Parameters.Add(Mapped.Parameter("?saida", reserva.Res_horario_saida));
            objCommand.Parameters.Add(Mapped.Parameter("?dt_cadastro", data));
            objCommand.Parameters.Add(Mapped.Parameter("?qtd_pessoas", reserva.Res_numero_pessoas));
            objCommand.Parameters.Add(Mapped.Parameter("?tipo_pagamento", reserva.Res_tipo_pagamento));
            objCommand.Parameters.Add(Mapped.Parameter("?quarto_id", reserva.Qua_quarto.Qua_id));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }

    public static int Insert(Quarto quarto)
    {

        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "INSERT INTO qua_quarto (qua_tipo, qua_status, qua_descricao, qua_num_usu_suportados , qua_banheiro, qua_chuveiro , qua_hidro , qua_brinquedos, qua_qt_cama, qua_valor) VALUES (?tipo, ?status, ?descricao, ?suportados, ?banheiro, ?chuveiro, ?hidro , ?brinquedo, ?cama, ?valor)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?tipo", quarto.Qua_tipo));
            objCommand.Parameters.Add(Mapped.Parameter("?status", quarto.Qua_status));
            objCommand.Parameters.Add(Mapped.Parameter("?descricao", quarto.Qua_descricao));
            objCommand.Parameters.Add(Mapped.Parameter("?suportados", quarto.Qua_num_usuportados));
            objCommand.Parameters.Add(Mapped.Parameter("?banheiro", quarto.Qua_banheiro));
            objCommand.Parameters.Add(Mapped.Parameter("?chuveiro", quarto.Qua_chuveiro));
            objCommand.Parameters.Add(Mapped.Parameter("?hidro", quarto.Qua_hidro));
            objCommand.Parameters.Add(Mapped.Parameter("?brinquedo", quarto.Qua_brinquedos));
            objCommand.Parameters.Add(Mapped.Parameter("?cama", quarto.Qua_qt_cama));
            objCommand.Parameters.Add(Mapped.Parameter("?valor", quarto.Qua_valor));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }

    //Select todos DDL ONDE SÓ APARECE OS DISPONIVEIS
    public static DataSet SelectAll()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM qua_quarto where qua_status = 1 ORDER BY qua_id", objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);

        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
        // DataSet com os dados do BD, O método Fill é o responsável por 
        // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;

    }

    //Select todos quartos
    public static DataSet SelectQuartos()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT qua_id, qua_tipo, CASE WHEN qua_status = '1' THEN 'Sim' ELSE 'Não' END qua_status, qua_descricao, qua_num_usu_suportados, qua_banheiro, qua_chuveiro, qua_hidro, qua_brinquedos, qua_qt_cama, qua_valor FROM qua_quarto ORDER BY qua_id", objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);

        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
        // DataSet com os dados do BD, O método Fill é o responsável por 
        // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;

    }


    //Search Na Grid
    public DataSet Search(int quarto)
    {
        DataSet ds = new DataSet();
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        System.Data.IDataAdapter objDataAdapter;
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM qua_quarto WHERE qua_id = ?id ORDER BY qua_id", objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?id", quarto));
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds);
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();
        return ds;
    }

    public bool UpdateStatusQuarto0(Quarto quarto)
    {
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        string sql = "UPDATE qua_quarto SET qua_status = '0' WHERE qua_id = ?id;";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?id", quarto.Qua_id));

        objCommand.ExecuteNonQuery();
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();
        return true;
    }//Update

    public bool UpdateStatusQuarto1(Quarto quarto)
    {
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        string sql = "UPDATE qua_quarto SET qua_status = '1' WHERE qua_id = ?id;";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?id", quarto.Qua_id));

        objCommand.ExecuteNonQuery();
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();
        return true;
    }//Update

}