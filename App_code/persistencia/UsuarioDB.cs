﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de UsuarioDB
/// </summary>
public class UsuarioDB
{

    public static int Insert(Usuario usuario)
    {
        DateTime data = DateTime.Now;
        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "INSERT INTO usu_usuario (usu_nome, usu_login, usu_senha, usu_dt_cadastro, tip_tipo_usuario_tip_tipo_id) VALUES (?usu_nome, ?usu_login, ?usu_senha, ?usu_dt_cadastro, ?tip_tipo_usuario_tip_tipo_id)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?usu_nome", usuario.Usu_nome));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_login", usuario.Usu_login));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_senha", usuario.Usu_senha));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_dt_cadastro", data));
            
            objCommand.Parameters.Add(Mapped.Parameter("?tip_tipo_usuario_tip_tipo_id", usuario.Tip_tipo_usuario_tip_tipo_id.Tip_tipo_id));

            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }


    public static DataSet SelectUsuarios()
    {
       
        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        
        objCommand = Mapped.Command("SELECT usu_id, usu_nome, usu_login, (Date_Format(usu_dt_cadastro,'%d/%m/%Y %Hh%i')) As Date FROM usu_usuario INNER JOIN tip_tipo_usuario ON tip_tipo_usuario_tip_tipo_id = tip_tipo_id  where tip_tipo_usuario_tip_tipo_id = 1", conexao: objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectClientes2()
    {

        DataSet ds = new DataSet();

        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT usu_id, usu_nome, usu_login, (Date_Format(usu_dt_cadastro,'%d/%m/%Y %Hh%i')) As Date FROM usu_usuario INNER JOIN tip_tipo_usuario ON tip_tipo_usuario_tip_tipo_id = tip_tipo_id  where tip_tipo_usuario_tip_tipo_id = 2", conexao: objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public DataSet SelectAll()
    {

        DataSet ds = new DataSet();

        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        System.Data.IDataAdapter objDataAdapter;

        objConexao = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM usu_usuario", objConexao);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds);

        objConexao.Close();

        objCommand.Dispose();
        objConexao.Dispose();

        return ds;

    }//SelectAll


    //Select todos quartos
    public static DataSet SelectClientes()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM cli_cliente", objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
        // DataSet com os dados do BD, O método Fill é o responsável por 
        // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;

    }

    public Usuario Select(int id)
    {

        Usuario obj = null;

        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        System.Data.IDataReader objDataReader;

        objConexao = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM usu_usuario WHERE usu_id = ?id", objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?id", id));

        objDataReader = objCommand.ExecuteReader();

        while (objDataReader.Read())
        {

            obj = new Usuario();
            obj.Usu_id = Convert.ToInt32(objDataReader["usu_id"]);
            obj.Usu_nome = Convert.ToString(objDataReader["usu_nome"]);
            obj.Usu_login = Convert.ToString(objDataReader["usu_login"]);
            obj.Usu_senha = Convert.ToString(objDataReader["usu_senha"]);


        }//While

        objDataReader.Close();
        objConexao.Close();

        objCommand.Dispose();
        objConexao.Dispose();
        objDataReader.Dispose();

        return obj;


    }//Select

    public bool Delete(int id)
    {
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        string sql = "DELETE FROM usu_usuario WHERE usu_id= ?id";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?id", id));
        objCommand.ExecuteNonQuery();
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();

        return true;
    }//Delete

    public bool Update(Usuario usuario)
    {
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        string sql = "UPDATE usu_usuario SET usu_nome = ?nome, usu_login = ?login, usu_senha = ?senha  WHERE usu_id= ?id";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);

        objCommand.Parameters.Add(Mapped.Parameter("?nome", usuario.Usu_nome));
        objCommand.Parameters.Add(Mapped.Parameter("?login", usuario.Usu_login));
        objCommand.Parameters.Add(Mapped.Parameter("?senha", usuario.Usu_senha));
        objCommand.Parameters.Add(Mapped.Parameter("?id", usuario.Usu_id));

        objCommand.ExecuteNonQuery();
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();
        return true;
    }//Update


    public static int UpdatePerfil(Usuario usuario)
    {

        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "UPDATE usu_usuario SET usu_nome = ?usu_nome, usu_login = ?usu_login, Tip_tipo_usuario_tip_tipo_id = ?Tip_tipo_usuario_tip_tipo_id WHERE usu_id = ?usu_id";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?usu_nome", usuario.Usu_nome));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_login", usuario.Usu_login));
            objCommand.Parameters.Add(Mapped.Parameter("?Tip_tipo_usuario_tip_tipo_id", usuario.Tip_tipo_usuario_tip_tipo_id.Tip_tipo_id));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_id", usuario.Usu_id));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }


    public static DataSet SelectLOGIN(string login, string senha)
    {
        DataSet ds = new DataSet(); IDbConnection objConexao; IDbCommand objCommand;
        IDataAdapter objDataAdapter; string sql = "select * from usu_usuario where usu_login = ?login and usu_senha = ?senha";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?login", login));
        objCommand.Parameters.Add(Mapped.Parameter("?senha", senha));
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds);
        objConexao.Close();
        objConexao.Dispose();
        objCommand.Dispose();
        return ds;
    }


}