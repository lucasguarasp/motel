﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Reserva
/// </summary>
public class Reserva
{
    private int res_id;
    private DateTime res_horario_entrada;
    private DateTime res_horario_saida;
    private DateTime res_dt_cadastro_reserva;
    private int res_numero_pessoas;
    private string res_tipo_pagamento;
    private Quarto qua_quarto;

    public int Res_id
    {
        get
        {
            return res_id;
        }

        set
        {
            res_id = value;
        }
    }

    public DateTime Res_horario_entrada
    {
        get
        {
            return res_horario_entrada;
        }

        set
        {
            res_horario_entrada = value;
        }
    }

    public DateTime Res_horario_saida
    {
        get
        {
            return res_horario_saida;
        }

        set
        {
            res_horario_saida = value;
        }
    }

    public DateTime Res_dt_cadastro_reserva
    {
        get
        {
            return res_dt_cadastro_reserva;
        }

        set
        {
            res_dt_cadastro_reserva = value;
        }
    }

    public int Res_numero_pessoas
    {
        get
        {
            return res_numero_pessoas;
        }

        set
        {
            res_numero_pessoas = value;
        }
    }

    public string Res_tipo_pagamento
    {
        get
        {
            return res_tipo_pagamento;
        }

        set
        {
            res_tipo_pagamento = value;
        }
    }

    public global:: Quarto Qua_quarto
    {
        get
        {
            return qua_quarto;
        }

        set
        {
            qua_quarto = value;
        }
    }
}