﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Usuario
/// </summary>
public class Usuario
{
    private int usu_id;
    private string usu_nome;
    private string usu_login;
    private string usu_senha;
    private TipoUsuario tip_tipo_usuario_tip_tipo_id;

    public int Usu_id
    {
        get
        {
            return usu_id;
        }

        set
        {
            usu_id = value;
        }
    }

    public string Usu_nome
    {
        get
        {
            return usu_nome;
        }

        set
        {
            usu_nome = value;
        }
    }

    public string Usu_login
    {
        get
        {
            return usu_login;
        }

        set
        {
            usu_login = value;
        }
    }

    public string Usu_senha
    {
        get
        {
            return usu_senha;
        }

        set
        {
            usu_senha = value;
        }
    }

    public global::TipoUsuario Tip_tipo_usuario_tip_tipo_id
    {
        get
        {
            return tip_tipo_usuario_tip_tipo_id;
        }

        set
        {
            tip_tipo_usuario_tip_tipo_id = value;
        }
    }
}