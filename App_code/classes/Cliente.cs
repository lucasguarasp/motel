﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Cliente
/// </summary>
public class Cliente
{
    private int cli_id;
    private string cli_nome;
    private string cli_sexo;
    private string cli_email;
    private string cli_telefone;

    public int Cli_id
    {
        get
        {
            return cli_id;
        }

        set
        {
            cli_id = value;
        }
    }

    public string Cli_nome
    {
        get
        {
            return cli_nome;
        }

        set
        {
            cli_nome = value;
        }
    }
    

    public string Cli_sexo
    {
        get
        {
            return cli_sexo;
        }

        set
        {
            cli_sexo = value;
        }
    }

    public string Cli_email
    {
        get
        {
            return cli_email;
        }

        set
        {
            cli_email = value;
        }
    }

    public string Cli_telefone
    {
        get
        {
            return cli_telefone;
        }

        set
        {
            cli_telefone = value;
        }
    }
}