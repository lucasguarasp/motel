﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de TipoUsuario
/// </summary>
public class TipoUsuario
{
    private int tip_tipo_id;
    private string tip_descicao;

    public int Tip_tipo_id
    {
        get
        {
            return tip_tipo_id;
        }

        set
        {
            tip_tipo_id = value;
        }
    }

    public string Tip_descicao
    {
        get
        {
            return tip_descicao;
        }

        set
        {
            tip_descicao = value;
        }
    }
}