﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Quarto
/// </summary>
public class Quarto
{

    private int qua_id;
    private string qua_tipo;
    private int qua_status;
    private string qua_descricao;
    private int qua_num_usuportados;
    private int qua_banheiro;
    private int qua_chuveiro;
    private int qua_hidro;
    private int qua_brinquedos;
    private int qua_qt_cama;
    private double qua_valor;

    public int Qua_id
    {
        get
        {
            return qua_id;
        }

        set
        {
            qua_id = value;
        }
    }

    public string Qua_tipo
    {
        get
        {
            return qua_tipo;
        }

        set
        {
            qua_tipo = value;
        }
    }

    
    public string Qua_descricao
    {
        get
        {
            return qua_descricao;
        }

        set
        {
            qua_descricao = value;
        }
    }

    public int Qua_num_usuportados
    {
        get
        {
            return qua_num_usuportados;
        }

        set
        {
            qua_num_usuportados = value;
        }
    }

    public int Qua_banheiro
    {
        get
        {
            return qua_banheiro;
        }

        set
        {
            qua_banheiro = value;
        }
    }

    public int Qua_chuveiro
    {
        get
        {
            return qua_chuveiro;
        }

        set
        {
            qua_chuveiro = value;
        }
    }

    public int Qua_hidro
    {
        get
        {
            return qua_hidro;
        }

        set
        {
            qua_hidro = value;
        }
    }

    public int Qua_brinquedos
    {
        get
        {
            return qua_brinquedos;
        }

        set
        {
            qua_brinquedos = value;
        }
    }

    public int Qua_qt_cama
    {
        get
        {
            return qua_qt_cama;
        }

        set
        {
            qua_qt_cama = value;
        }
    }

    public double Qua_valor
    {
        get
        {
            return qua_valor;
        }

        set
        {
            qua_valor = value;
        }
    }

    public int Qua_status
    {
        get
        {
            return qua_status;
        }

        set
        {
            qua_status = value;
        }
    }
}