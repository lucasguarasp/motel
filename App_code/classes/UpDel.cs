﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de UpDel
/// </summary>
public class UpDel
{
    static int index, id;

    public static int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }

    public static int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }
}